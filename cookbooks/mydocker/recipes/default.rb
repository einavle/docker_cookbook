# frozen_string_literal: true
# This is a Chef recipe file. It can be used to specify resources which will
# apply configuration to a server.

# For more information, see the documentation: https://docs.chef.io/recipes.html


#yum_package "docker-ce" do
#  action :install
#  version nil
#end

#service 'docker' do
#  supports :status => true, :restart => true, :reload => true
#  action   [:start, :enable]
#end


#docker_registry "#{node['docker_registry']['url']}" do
#  username node['docker_registry']['username']
#  password node['docker_registry']['password']
#  email node['docker_registry']['email']
#end


docker_image 'webapp' do
  repo "einavl/webapp"
  tag node['docker']['image']['version']
  action :pull
  read_timeout node['docker_registry']['read_timeout']
end




#starting it.
docker_container 'webapp' do
  repo "einavl/webapp"
  tag node['docker']['image']['version']
  action :run
  port node['docker']['container']['webapp_port']
  tty true
  read_timeout node['docker_registry']['read_timeout']
  restart_policy node['docker_registry']['restart_policy']
  restart_maximum_retry_count node['docker_registry']['restart_maximum_retry_count']
end