# frozen_string_literal: true
# This is a Chef attributes file. It can be used to specify default and override
# attributes to be applied to nodes that run this cookbook.

# Set a default name
default['docker']['image']['version'] = 'REPLACEME REPLACE-ME'
default['docker_registry']['read_timeout'] = 30
default['docker']['container']['webapp_port'] = ["80:8080"]
default['docker_registry']['restart_policy'] = "on-failure"
default['docker_registry']['restart_maximum_retry_count'] = 4

# For further information, see the Chef documentation (https://docs.chef.io/attributes.html).
