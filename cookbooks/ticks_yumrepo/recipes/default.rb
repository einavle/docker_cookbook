#
# Cookbook Name:: ticks_yumrepo
# Recipe:: default
#


template "influxdb.repo" do
  source "influxdb.erb"
  path "/etc/yum.repos.d/influxdb.repo"
  action :create  
end
